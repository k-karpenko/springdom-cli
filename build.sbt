name := "springdom-env"

version := "1.0"
scalaVersion := "2.11.7"

val akkaVersion = "2.3.12"
val logbackVersion = "1.1.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence-experimental" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-experimental" % "1.0",
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",
  "org.scalatest"     %% "scalatest" % "2.2.4" % "test",
  "ch.qos.logback"    % "logback-core" % logbackVersion,
  "ch.qos.logback"    % "logback-classic" % logbackVersion,
  "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
  "com.michaelpollmeier" %% "gremlin-scala" % "3.0.0-incubating",
  "com.michaelpollmeier" % "orientdb-gremlin" % "3.0.0.M1a",
  "com.orientechnologies" % "orientdb-graphdb" % "2.1-rc5",
  "org.scalatest"     %% "scalatest" % "2.2.4" % "test",
  "com.assembla.scala-incubator" %% "graph-core" % "1.9.4"
)