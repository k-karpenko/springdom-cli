package nz.springdom.cli

sealed trait Operation
object Operation {

  object Exit {
    val Name = "exit"
    case object Op extends Operation
  }

  object Connect {
    val Name = "connect"
    case object Op extends Operation
  }

  object Message {
    val Name = "message"
    case class Op(message: String) extends Operation
  }
}
