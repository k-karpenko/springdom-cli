package nz.springdom.cli

import scala.util.{Failure, Try}

trait InputParser {
  def extractOperation(input: String): Try[Operation]
}

object InputParser {

  case object EmptyInput extends Exception
  case object MissingValue extends Exception
  case object UnknownOperation extends Exception

  def apply() = new InputParserImpl()

  private[InputParser] class InputParserImpl extends InputParser {
    override def extractOperation(input: String): Try[Operation] = Try {
      val firstSpacePosition = input.indexOf(" ")
      val operation =
          if(firstSpacePosition != -1) input.substring(0, firstSpacePosition) else input

      operation match {
        case Operation.Exit.Name ⇒ Operation.Exit.Op
        case Operation.Connect.Name ⇒ Operation.Connect.Op
        case Operation.Message.Name ⇒
          if ( firstSpacePosition == -1 ) throw MissingValue
          else Operation.Message.Op(input.substring(firstSpacePosition + 1))
        case _ ⇒ throw UnknownOperation
      }
    }
  }

}
