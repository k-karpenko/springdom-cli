package nz.springdom.cli

sealed trait Output
object Output {
  case class Error(message: String) extends Output
  case class Print(message: String) extends Output
  case object Empty extends Output
  case object Exit extends Output
}
