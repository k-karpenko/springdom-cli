package nz.springdom.cli.actors

import akka.actor.{Props, ActorRef, Actor}
import akka.util.Timeout
import com.typesafe.scalalogging.slf4j.LazyLogging
import nz.springdom.cli.{Operation, Output, InputParser}
import scala.concurrent.Future
import scala.util.{Failure, Success}
import akka.pattern.{ask, pipe}

import scala.concurrent.duration._

import scala.language.postfixOps

object InputProcessingActor {
  import nz.springdom.cli.actors.CommandExecutorActor.FailureType

  private[InputProcessingActor] val connectedMessageText = "Connection with remote host has been established!"
  private[InputProcessingActor] val messageSentText = "Message has been delivered"

  private[InputProcessingActor] val failureTexts = Map[FailureType, String](
    FailureType.AlreadyConnected → "Connection has already been established",
    FailureType.DeliveryFailed → "Failure while delivering message",
    FailureType.NotConnected → "First, you need to establish connection by calling 'connect' operation"
  )

  def props(inputParser: InputParser, commandExecutor: ActorRef): Props = {
    Props(classOf[InputProcessingActor], inputParser, commandExecutor)
  }

  sealed trait Event
  object Event {
    case class OutputEvent(output: Output) extends Event
  }

  sealed trait Request
  object Request {
    case class Input(value: String) extends Request
  }

  sealed trait Response
  object Response {
    case class Result(output: Output) extends Response
  }
}

class InputProcessingActor(inputParser: InputParser,
                           commandExecutor: ActorRef) extends Actor with LazyLogging {
  import InputProcessingActor._

  implicit val ec = context.dispatcher
  implicit val timeout = Timeout(300 millis)


  override def preStart(): Unit = {
    super.preStart()

    context.system.eventStream.subscribe(self, classOf[CommandExecutorActor.Event])
  }

  override def receive: Receive = {
    case r: CommandExecutorActor.Event.StatusEvent ⇒
      context.system.eventStream.publish(
        InputProcessingActor.Event.OutputEvent(Output.Print(s"= Delivered ${r.deliveredTotal}, last sent ${r.lastDeliveryDate}"))
      )

    case Request.Input(msg) ⇒
      val future = inputParser.extractOperation(msg) match {
        case Success( Operation.Connect.Op ) ⇒
          (commandExecutor ? CommandExecutorActor.Request.ConnectRequest).map {
            case CommandExecutorActor.Response.Connected ⇒ Output.Print(connectedMessageText)
            case CommandExecutorActor.Response.Failure(error) ⇒ Output.Error(failureTexts(error))
          }

        case Success( Operation.Exit.Op ) ⇒
          (commandExecutor ? CommandExecutorActor.Request.Disconnect).map {
            case CommandExecutorActor.Response.Disconnected ⇒ Output.Exit
            case CommandExecutorActor.Response.Failure(error) ⇒ Output.Error(failureTexts(error))
          }

        case Success( Operation.Message.Op(message) ) ⇒
          (commandExecutor ? CommandExecutorActor.Request.MessageRequest(message)).map {
            case CommandExecutorActor.Response.Sent ⇒ Output.Print(messageSentText)
            case CommandExecutorActor.Response.Failure(error) ⇒ Output.Error(failureTexts(error))
          }
        case Failure(InputParser.MissingValue) ⇒ Future.successful(Output.Error("Missing value"))
        case Failure(InputParser.EmptyInput) ⇒ Future.successful(Output.Error("Operation name is missing"))
        case Failure(InputParser.UnknownOperation) ⇒ Future.successful(Output.Error("Unknown operation requested"))
        case Failure(_) ⇒ Future.successful(Output.Error("Unknown exception"))
      }

      future pipeTo sender()
  }
}
