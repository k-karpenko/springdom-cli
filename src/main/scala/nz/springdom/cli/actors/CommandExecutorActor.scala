package nz.springdom.cli.actors

import java.util.Date

import akka.actor.{Cancellable, FSM, Props}
import akka.pattern.pipe
import nz.springdom.cli.{Connection, ConnectionProvider}

import scala.concurrent.duration._
import scala.language.postfixOps

object CommandExecutorActor {
  def props(implicit connectionProvider: ConnectionProvider): Props = {
    Props(classOf[CommandExecutorActor], connectionProvider)
  }

  sealed trait FailureType
  object FailureType {
    case object NotConnected extends FailureType
    case object AlreadyConnected extends FailureType
    case object DeliveryFailed extends FailureType
  }

  sealed trait Request
  object Request{
    case object ConnectRequest extends Request
    case class MessageRequest(message: String) extends Request
    case object Disconnect extends Request
  }

  sealed trait Response
  object Response {
    case object Sent extends Response
    case object Connected extends Response
    case class Failure(tpe: FailureType) extends Response
    case object Disconnected extends Response

  }

  private[CommandExecutorActor] sealed trait Internal
  private[CommandExecutorActor] object Internal {
    case object StatusPublishTicket extends Internal
  }

  private[CommandExecutorActor] sealed trait State
  private[CommandExecutorActor] object State {
    case object NotConnected extends State
    case object Connected extends State
  }

  private[CommandExecutorActor] sealed trait Data
  private[CommandExecutorActor] object Data {
    case object Empty extends Data
    case class ConnectionData(connection: Connection, updateTimer: Cancellable, delivered: Int = 0,
                              lastDeliveryDate: Date = new Date(), connected: Date = new Date() ) extends Data
  }

  sealed trait Event
  object Event {
    case class StatusEvent(deliveredTotal: Int, lastDeliveryDate: Date, startedOn: Date) extends Event
  }

}

class CommandExecutorActor(implicit connectionProvider: ConnectionProvider)
  extends FSM[CommandExecutorActor.State, CommandExecutorActor.Data] {
  import CommandExecutorActor.Data._
  import CommandExecutorActor.State._
  import CommandExecutorActor._

  startWith(NotConnected, Empty)

  implicit val ec = context.dispatcher

  when(NotConnected) {
    case Event(Request.ConnectRequest, _) ⇒
      val statusTicker = context.system.scheduler.schedule(3 seconds, 10 seconds, self,
        CommandExecutorActor.Internal.StatusPublishTicket)

      /**
       * todo: we assume here that connection will never be closed or will restore its state by itself, so all
       *       CommandExecutorActor operations will be valid
       */
      goto(Connected) using ConnectionData(connectionProvider.provide(), statusTicker) replying Response.Connected

    case Event(Request.MessageRequest(_), _) ⇒
      stay() replying Response.Failure(FailureType.NotConnected)
    case Event(Request.Disconnect, _) ⇒
      stay() replying Response.Disconnected
  }

  when(Connected) {
    case Event(Internal.StatusPublishTicket, ConnectionData(_, _, delivered, lastDelivery, connected)) ⇒
      context.system.eventStream.publish( CommandExecutorActor.Event.StatusEvent(delivered, lastDelivery, connected))
      stay()

    case Event(Request.ConnectRequest, _) ⇒
      stay() replying Response.Failure(FailureType.AlreadyConnected)

    case Event(Request.MessageRequest(msg), c @ ConnectionData(connection, _, _,  _,  _)) ⇒
      val sendFuture = connection
          .send(msg)
          .map (r ⇒ Response.Sent)
          .recover {
            case e: Throwable ⇒ Response.Failure(FailureType.DeliveryFailed)
          }

      sendFuture pipeTo sender()

      stay() using c.copy(delivered = c.delivered + 1, lastDeliveryDate = new Date())
    case Event(Request.Disconnect, ConnectionData(connection, _, _, _, _)) ⇒
      connection.disconnect()
      goto(NotConnected) using Empty replying Response.Disconnected
  }


  initialize()

}
