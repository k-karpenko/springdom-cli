package nz.springdom.cli

import scala.concurrent.{ExecutionContext, Future}

trait ConnectionProvider {
  def provide()(implicit ec: ExecutionContext): Connection
}

object ConnectionProvider {
  def apply(): ConnectionProvider = new ConnectionProviderImpl

  private[ConnectionProvider] class ConnectionProviderImpl extends ConnectionProvider {
    override def provide()(implicit ec: ExecutionContext): Connection = Connection()
  }
}

trait Connection {
  type Response = String
  type Request = String

  def send(request: Request)(implicit ec: ExecutionContext): Future[Response]
  def disconnect()(implicit ec: ExecutionContext): Future[Unit]
}

object Connection {

  def apply()(implicit ec: ExecutionContext) = new ConnectionImpl()

  private[Connection] class ConnectionImpl extends Connection {
    override def disconnect()(implicit ec: ExecutionContext): Future[Unit] = {
      Future {}
    }

    override def send(message: Request)(implicit ec: ExecutionContext): Future[Response] = {
      Future {
        message.reverse
      }
    }
  }

}
