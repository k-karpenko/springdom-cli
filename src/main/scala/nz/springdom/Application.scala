package nz.springdom

import akka.actor.{Actor, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import nz.springdom.cli.actors.{CommandExecutorActor, InputProcessingActor}
import nz.springdom.cli.{ConnectionProvider, InputParser, Output}

import scala.annotation.tailrec
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}
import scala.language.postfixOps

object Application {

  val defaultOperationTimeout = 30.seconds

  implicit val actorSystem = ActorSystem("cli-actor-system")

  implicit val ec = ExecutionContext.Implicits.global

  implicit val connectionProvider = ConnectionProvider()

  val inputParser = InputParser()

  val eventReactorRef = actorSystem.actorOf(Props(new Actor {
    override def receive: Receive = {
      case InputProcessingActor.Event.OutputEvent(output) ⇒
        renderOutputAndDecide(output)
    }
  }))

  val processorActorRef = actorSystem.actorOf(CommandExecutorActor.props)
  val inputProcessingActorRef = actorSystem.actorOf(
    InputProcessingActor.props(inputParser, processorActorRef))

  def main( args: Array[String] ): Unit = {
    Console.out.println("Welcome!")
    listenForInput()
  }

  @tailrec
  def listenForInput(): Unit = {
    Console.out.print(">> ")
    val input = Console.in.readLine().trim()

    implicit val timeout = Timeout(defaultOperationTimeout)

    actorSystem.eventStream.subscribe( eventReactorRef, classOf[InputProcessingActor.Event])

    val future = (inputProcessingActorRef ? InputProcessingActor.Request.Input(input)).mapTo[Output]
    val result = Await.result(future, defaultOperationTimeout)
    
    val continue = renderOutputAndDecide(result)
    if ( continue ) {
      listenForInput()
    } else {
      actorSystem.shutdown()
    }
  }

  private def renderOutputAndDecide(output: Output): Boolean = {
    val decision = output match {
      case Output.Exit ⇒
        Console.out.println(". Goodbye!")
        false
      case Output.Error(msg) ⇒
        Console.err.println(s">> [ERROR] $msg")
        true
      case Output.Print(msg) ⇒
        Console.out.println(s">> $msg")
        true
      case Output.Empty ⇒
        true
    }

    System.out.println()
    decision
  }

}
